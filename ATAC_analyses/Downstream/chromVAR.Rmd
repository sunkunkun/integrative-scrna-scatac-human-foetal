---
title: "chromVAR analysis"
output: html_notebook
---

```{r}
library(Matrix)
library(tidyverse)
library(chromVAR)
library(motifmatchr)
library(SummarizedExperiment)
library(BiocParallel)
set.seed(2017)
library(TFBSTools)
library(pheatmap)
library(data.table)
library(irlba)
library(Seurat)
library(Signac)
library(harmony)
library(BSgenome.Hsapiens.UCSC.hg38)
library(EnsDb.Hsapiens.v86)

```

# Load peaks and Seurat object

```{r}
# overlap all peaks 
# get the couns matrix 
peakfile <- "/datadisk/Desktop/Projects/SCRNATAC/scATAC/peaks/snapatac.clust.peaks.bed"

peaks.df = as.data.frame(read_tsv(peakfile, col_names = F))
peaks.df$ID = paste0(peaks.df$X1,":",peaks.df$X2,"-",peaks.df$X3)
rownames(peaks.df) = peaks.df$ID
# peakset = "consensus"
peaks <- getPeaks(peakfile, sort_peaks = TRUE)



# genome coordinates for binning 
genome <- BSgenome.Hsapiens.UCSC.hg38
seq.gen  = seqlengths(genome)

 test = readRDS(file = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/files/test.merged234.Snap.HA.Rds")
```

# Run ChromVAR

```{r}

DefaultAssay(test) = "peaks"
# run chromVAR 

# prepare motifs 
 motifs_name = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/HOCO11HUMAN.pfmlist.rds"
if (file.exists(motifs_name)) {
  motifs <- readRDS(motifs_name)
} else {
  library(universalmotif)
  library(TFBSTools)
  library(rlist)
  # data taken from https://hocomoco11.autosome.ru/final_bundle/hocomoco11/full/HUMAN/mono/HOCOMOCOv11_full_pwm_HUMAN_mono.tar.gz
  # for file in pwm/*pwm; do echo "" >> "$file"; done
  pathToPWMs = "/scratch/berest/SC/CVEJIC/pwm/"
  pwmfiles = list.files(pathToPWMs)

  empty.list <- list()

  for (x in 1:length(pwmfiles)) {
  
    test1 = read_matrix(paste0(pathToPWMs,pwmfiles[x]), 
                      headers = ">", sep = "", positions = "rows")
    lel = convert_motifs(test1, class = "TFBSTools-PFMatrix")
  
    empty.list <- c(empty.list, lel)
  
  }

  pfm.list <- do.call(PFMatrixList, empty.list)

  list.save(pfm.list, file = motifs_name)
  
  motifs <- readRDS(motifs_name)
}

 
 

 # # Scan the DNA sequence of each peak for the presence of each motif
 
 motif.matrix_name = "/g/scb2/zaugg/berest/Projects/collaborations/SCRNAATAC/scratch/CVEJIC/test/motifmatrix.merged234snap.Rds"
 
 if (file.exists(motif.matrix_name)) {
   motif.matrix = readRDS(file  = motif.matrix_name)
 } else {
   
    motif.matrix <- CreateMotifMatrix(
      features = StringToGRanges(rownames(test), sep = c("-", "-")),
      pwm = motifs,
      genome = 'hg38',
      sep = c("-", "-"),
      use.counts = FALSE
    )
    colnames(motif.matrix) = TFBSTools::name(motifs)
    saveRDS(motif.matrix, file  = motif.matrix_name)

    motif.matrix = readRDS(file  = motif.matrix_name)

 }

# Create a new Mofif object to store the results
motif <- CreateMotifObject(
  data = motif.matrix,
  pwm = motifs
)

# Add the Motif object to the assay
test[['peaks']] <- AddMotifObject(
  object = test[['peaks']],
  motif.object = motif
)



test <- RegionStats(
  object = test,
  genome = BSgenome.Hsapiens.UCSC.hg38,
  sep = c("-", "-")
)


## actual running chromVar 
test <- RunChromVAR(
  object = test,
  genome = BSgenome.Hsapiens.UCSC.hg38,
  assay = "peaks"
)
``` 

```{r}
library(ggrepel)
library(rlist)
library(plotly)
deviations = as.matrix(GetAssayData(test, assay = "chromvar"))

deviations.s = deviations[order(rowSds(deviations), decreasing = T),]


nx = 20
ggplot() + geom_point(aes(y = rowSds(deviations.s), x = 1:nrow(deviations.s), label = rownames(deviations.s))) + 
  geom_label_repel(aes(y = rowSds(deviations.s[1:nx,]), x = 1:nrow(deviations.s[1:nx,]), 
                       label = unlist(str_split(rownames(deviations.s)[1:nx],"-",simplify =  F) %>% list.map(.[1])) ))


plot_ly(y = rowSds(deviations.s), x = 1:nrow(deviations.s),color = "black", name = unlist(str_split(rownames(deviations.s),"-",simplify =  F) %>% list.map(.[1]))
) %>% layout(showlegend = FALSE)

 deviations.s.plot = deviations.s[rowSds(deviations.s) > 1.2,]
 nrow(deviations.s.plot)
```

```{r}
 saveRDS(test, file = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/files/test.merged234.Snap.HA.ChromVar.Rds")
```

